
public class Konsolenausgabe {

	public static void main(String[] args) {

		//Aufagbe 1
		// Bei println() gibt es einen Zeilenvorschub. Desweiteren sind Steuerzeichen nutzbar.
		System.out.println("Hello World. My name is Anne. \nThis makes my angry. \n"
				+ "Dies sind Beispielsätze \n"
				+ " ");

		//Aufgabe 2
		System.out.println("      *      \n"
				+ "     ***     \n"
				+ "    *****   \n"
				+ "   ******* \n"
				+ "  *********\n"
				+ " ***********\n"
				+ "*************\n"
				+ "     ***\n"
				+ "     ***");
		
		//Aufgabe 3
		
		System.out.printf("|%.2f|\n|%.2f|\n|%.2f|\n|%.2f|\n|%.2f|" , 22.4234234, 111.2222, 4.0, 1000000.551, 97.34);
		
	}
}
